<?php

if (!file_exists(dirname(__FILE__) . '/digiview.db')) {
    $db = new PDO('sqlite:'. dirname(__FILE__) . '/digiview.db');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $table = "CREATE TABLE digiview_videos (
        id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        url TEXT NOT NULL,
        videoId TEXT NOT NULL,
        titre TEXT NOT NULL,
        description	TEXT NOT NULL,
        vignette TEXT NOT NULL,
        debut INTEGER NOT NULL,
        fin	INTEGER NOT NULL,
        largeur	INTEGER NOT NULL,
        hauteur	INTEGER NOT NULL,
        sousTitres TEXT NOT NULL,
        date TEXT NOT NULL,
        vues INTEGER NOT NULL,
        derniere_visite TEXT NOT NULL,
        digidrive INTEGER NOT NULL
    )";
    $db->exec($table);
} else {
    $db = new PDO('sqlite:'. dirname(__FILE__) . '/digiview.db');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $db->prepare('PRAGMA table_info(digiview_videos)');
	if ($stmt->execute()) {
        $tables = $stmt->fetchAll();
        if (count($tables) < 14) {
            $colonne = "ALTER TABLE digiview_videos ADD vues INTEGER NOT NULL DEFAULT 0";
            $db->exec($colonne);
            $colonne = "ALTER TABLE digiview_videos ADD derniere_visite TEXT NOT NULL DEFAULT ''";
            $db->exec($colonne);
        } else if (count($tables) === 14) {
            $colonne = "ALTER TABLE digiview_videos ADD digidrive INTEGER NOT NULL DEFAULT 0";
            $db->exec($colonne);
        }
    }
}

?>
